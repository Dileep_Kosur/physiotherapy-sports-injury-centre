package Entities;

import DataAccess.DataAccess;

public class Patient extends Personnel{

    private String address;
    private String telephone;

    public Patient(String name,String address,String telephone) {    	
        super(DataAccess.getPatientCounter(),name);
        this.address = address;
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public String getTelephone() {
        return telephone;
    }
}
