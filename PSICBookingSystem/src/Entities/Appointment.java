package Entities;

import DataAccess.DataAccess;

public class Appointment {
    private int id;
    private int treatmentId;
    private int date;
    private int hour;
    private int physicianId;
    private int patientId;
    private boolean isVisitor;
    private String notes;

    public Appointment(int treatmentId,
                       int date,
                       int hour,
                       int physicianId,
                       int patientId,
                       boolean isVisitor,String notes) {
        this.id = DataAccess.getAppointmentCounter();
        this.treatmentId = treatmentId;
        this.date = date;
        this.hour = hour;
        this.physicianId = physicianId;
        this.patientId = patientId;
        this.isVisitor = isVisitor;
        this.notes=notes;
    }

    public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getId() {
        return id;
    }

    public int getTreatmentId() {
        return treatmentId;
    }


    public int getPhysicianId() {
        return physicianId;
    }

    public int getDate() {
        return date;
    }

    public int getHour() {
        return hour;
    }

    public int getPatientId() {
        return patientId;
    }

    public boolean isVisitor() {
        return isVisitor;
    }
}
