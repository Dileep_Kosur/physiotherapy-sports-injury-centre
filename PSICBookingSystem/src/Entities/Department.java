package Entities;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import DataAccess.DataAccess;


public class Department {

    private int id;
    private String name;
    private List<Treatment> treatmentsAvailable;

    public Department(String name, List<String> treatmentsAvailable) {
        this.id = DataAccess.getDepartmentCounter();
        this.name = name;
        this.treatmentsAvailable = new ArrayList<>();
        for(String string : treatmentsAvailable){
            String[] data = string.split("-");
            this.treatmentsAvailable.add(new Treatment(data[0],data[1]));
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Treatment[] getTreatmentsAvailable() {
        Treatment[] treatments = new Treatment[treatmentsAvailable.size()];
        for(int i=0;i< treatmentsAvailable.size();i++){
            treatments[i] = treatmentsAvailable.get(i);
        }
        return treatments;
    }

}
