package Entities;

import DataAccess.DataAccess;

public class Visitor extends Personnel{
    public Visitor(String name) {
        super(DataAccess.getVisitorCounter(),name);
    }
}
