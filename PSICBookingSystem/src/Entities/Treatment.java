package Entities;

import DataAccess.DataAccess;

public class Treatment {
    private int id;
    private String name;
    private String room;

    public Treatment(String name, String room) {
        this.id = DataAccess.getTreatmentCounter();
        this.name = name;
        this.room = room;
    }

    public String getName() {
        return name;
    }

    public String getRoomName() {
        return room;
    }

    public int getId() {
        return id;
    }
}
