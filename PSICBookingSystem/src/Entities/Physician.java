package Entities;

import Utils.Parser;

import java.util.ArrayList;
import java.util.List;

import DataAccess.DataAccess;

public class Physician extends Personnel{

    private String address;
    private String telephone;
    private int[] areaOfExpertiseIds;
    private boolean[][] consultationHours;
    private String consultationHoursString;

    public Physician(String name,
                     String address,
                     String telephone,
                     int[] areaOfExpertiseIds,
                     String consultationHoursString) throws Exception {
        super(DataAccess.getPhysicianCounter(),name);

        this.consultationHoursString = consultationHoursString;
        this.consultationHours = Parser.parseConsultationHours(consultationHoursString,31);

        this.address = address;
        this.telephone = telephone;
        this.areaOfExpertiseIds = areaOfExpertiseIds;
    }

    public boolean isExpertise(int id){
        for(int departmentId : areaOfExpertiseIds){
            if(id == departmentId) return true;
        }
        return false;
    }


    public String getAddress() {
        return address;
    }

    public String getTelephone() {
        return telephone;
    }

    public int[] getAreaOfExpertise() {
        return areaOfExpertiseIds;
    }

    public boolean[][] getConsultationHours() {
        return consultationHours;
    }

    public String getConsultationHoursString(){
        return consultationHoursString;
    }

    public List<Integer> getSlotsAvailable(int date){
        if(date <0 || date > 30){
            return new ArrayList<>();
        }
        List<Integer> availableSlots = new ArrayList<>();
        for(int hr = 0; hr < 24; hr++){
            if(consultationHours[date][hr]){
                availableSlots.add(hr);
            }
        }
        return availableSlots;
    }

    public String toString(){
        String s = "";
        s += "{\n\tid: "+getId()+",\n";
        s += "\tname: "+getName()+",\n";
        s += "\taddress: "+address+",\n";
        s += "\ttelephone: "+telephone+",\n";
        s += "\tareas of expertise: {\n";
        for(int department : areaOfExpertiseIds){
            s += "\t\t"+department+",\n";
        }
        s = s.substring(0,s.length()-2) + "\n";
        s += "\t}\n}";
        return s;
    }

    public boolean blockSlot(int date, int hour) {
        if(consultationHours[date][hour]){
            consultationHours[date][hour] = false;
            return true;
        }
        return false;
    }

    public void unblockSlot(int date, int hour) {
        consultationHours[date][hour] = true;
    }

}
