package Entities;

public abstract class Personnel {
    private int id;
    private String name;
    public Personnel(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
