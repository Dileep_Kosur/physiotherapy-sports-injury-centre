package JUnitTests;

import DataAccess.DataAccess;
import Entities.Appointment;
import Entities.Patient;
import Entities.Physician;
import org.junit.Assert;
import org.junit.Test;

public class JUnitTests {

    DataAccess dataAccess = new DataAccess();

    public JUnitTests() throws Exception {
    }

    @Test
    public void testLoadedPatient(){
        assert(dataAccess.getPatients().size() > 0);
    }

    @Test
    public void testRegisterPatient(){
        Patient patient = new Patient("patient1","address1","9090909090");
        dataAccess.registerPatient(patient.getName(), patient.getAddress(), patient.getTelephone());

        //Patients should not be equal because ids are different, as data access creates a new instance of patient
        Assert.assertNotEquals(patient,dataAccess.getPatientByName(patient.getName()));

        //But the name, telephone and address should be equal
        Assert.assertEquals(patient.getName(),dataAccess.getPatientByName(patient.getName()).getName());
        Assert.assertEquals(patient.getAddress(),dataAccess.getPatientByName(patient.getName()).getAddress());
        Assert.assertEquals(patient.getTelephone(),dataAccess.getPatientByName(patient.getName()).getTelephone());
    }

    @Test
    public void testLoadedPhysician(){
        assert(dataAccess.getPhysicians().size() > 0);
    }

    @Test
    public void testLoadedDepartment(){
        assert(dataAccess.getDepartmentList().size() > 0);
    }

    @Test
    public void testFetchData() throws Exception {
        dataAccess.registerPatient("testPatient", "testAddress", "646-739-7186");
        dataAccess.registerPhysician("testPhysician","testAddress","646-739-7186",new int[]{},"2pm-6pm");

        Patient patient = dataAccess.getPatientByName("testPatient");
        Physician physician = dataAccess.getPhysicianByName("testPhysician");

        Assert.assertNotNull(patient);
        Assert.assertNotNull(physician);

    }

    @Test
    public void testPhysicianConsultationHours() throws Exception {
        Physician physician = new Physician(
                "testName",
                "testAddress, testState",
                "646-739-7186",
                new int[]{},
                "2pm-5pm"   //daily times of physician (repeated 7 times for one week)
        );

        //Should not be able to block slot because the physician is not available at given time
        Assert.assertEquals(physician.blockSlot(12,0),false);

        //Should br able to block slot the physician is available at given time
        assert(physician.blockSlot(12,15));
    }

    @Test
    public void testBookInvalidAppointment(){
        int patientId = 1;
        int physicianId = -1;   //Invalid physician ID

        Appointment appointment = dataAccess.bookAppointment(patientId,"Test",physicianId,1,1,1,false);

        //Appointment is invalid, so not successfully booked
        Assert.assertNull(appointment);

    }

    @Test
    public void testBookUnavailableAppointment() throws Exception {
        int patientId = 1;

        int physicianId = dataAccess.registerPhysician("testName",
                "testAddress","646-739-7186",new int[]{},"2pm-5pm");

        //Physician valid, but not available at the given slot
        Appointment appointment = dataAccess.bookAppointment(patientId,"Test",physicianId,1,1,1,false);

        Assert.assertNull(appointment);
    }

    @Test
    public void testBookAvailableAppointment() throws Exception {
        int patientId = 1;

        int physicianId = dataAccess.registerPhysician("testName",
                "testAddress","646-739-7186",new int[]{},"2pm-5pm");

        //Physician valid and slot available
        Appointment appointment = dataAccess.bookAppointment(patientId,"Test",physicianId,1,1,15,false);

        Assert.assertNotNull(appointment);
    }

    @Test
    public void testCancelAppointment() throws Exception {

        //There are no cancelled appointments, so size of the list should be 0
        Assert.assertEquals(0,dataAccess.getCancelledAppointments().size());

        int patientId = dataAccess.registerPatient("testName","testAddress","646-739-7186");

        int physicianId = dataAccess.registerPhysician("testName",
                "testAddress","646-739-7186",new int[]{},"2pm-5pm");

        Appointment appointment = dataAccess.bookAppointment(patientId,"Test",physicianId,1,1,15,false);

        //Since appointment is valid, it is not null
        Assert.assertNotNull(appointment);

        //Cancel the appointment
        dataAccess.cancelAppointment(appointment);

        //Now there is a cancelled appointment, so size is not zero
        Assert.assertNotEquals(0,dataAccess.getCancelledAppointments().size());
    }




}
