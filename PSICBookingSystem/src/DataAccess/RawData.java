package DataAccess;

import Entities.Physician;

import java.util.List;

public class RawData {
    public static String physicianData(){
        return "John Walker;11 Monroe Avenue, Luton;9416773786;2pm-4pm,3pm-6pm,1pm-5pm,2pm-4pm,3pm-6pm,1pm-5pm,1pm-4pm;Physiotherapy;Osteopathy\n" +
                "Ray Hoster;18 Madison Ave, New York;6467397186;2pm-6pm;Rehabilitation;Physiotherapy\n" +
                "Theodore Schwartz;105 York Ave London;8798625938;12pm-7pm;Osteopathy\n" +
                "Cahill Kevin M;85 5th Ave, London;6467397186;2pm-4pm,3pm-6pm,1pm-5pm,2pm-4pm,3pm-6pm,2pm-4pm,10am-6pm;Rehabilitation;Physiotherapy\n"+
                "Singh;42 York Ave, Hatfield;8798625938;6pm-8pm;Osteopathy;Physiotherapy";

    }
    public static String departmentData(){
        return "Physiotherapy;Electrotherapy-Medical consulting suite A;Ultrasound-Medical consulting suite A;Heat Therapy-Medical consulting suite B;Neural Mobilisation-Medical consulting suite C\n" +
                "Osteopathy;Structural Osteopathy-Medical consulting suite B;Classical Osteopathy-Medical consulting suite A;Cranial Osteopathy-Medical consulting suite C\n" +
                "Rehabilitation;Physical Exercise-Ground;Gym-Gym room;Swimming Exercises-Swimming Pool";
    }
    public static String patientData(){
        return "Tom;3  Vineyard Drive, Hatfield;4404578928\n" +
                "Messi;15  Monroe Avenue, Luton;9416773786\n" +
                "Figo;17 Harter Street, Chennai;9376672566\n" +
                "Tammy;36, Snyder Avenue, New York;7047132071\n" +
                "Bray;6  Butternut Lane, DC;6184786230\n" +
                "Marie;10  Forest Avenue, NJ;6467397186\n" +
                "Robert D;18  Poe Lane;9134932956\n" +
                "Emma;54  Caynor Circle, London;2012597257\n" +
                "Leroy G Stone;77  John Avenue, Mumbai;5172567603\n" +
                "Chriss;3654 SA, NC;6184786230\n" +
                "Vinay;3  Vineyard Drive, Hatfield;4404578928\n" +
                "Vas;15  Monroe Avenue, Luton;9416773786\n" +
                "Siva;17 Harter Street, Chennai;9376672566\n" +
                "Jon;36, Snyder Avenue, New York;7047132071\n" +
                "Test;6  Butternut Lane, DC;6184786230\n";
    }
    public static String appointmentData() {
    	return "1;10;14;4;5;false\n"+
    			"2;5;18;1;5;false\n"+
    			"3;3;16;2;15;false\n"+
    			"4;12;14;3;6;false\n"+
    			"1;17;14;4;2;false\n"+
    			"2;25;18;2;1;false\n"+
    			"3;30;16;5;6;false\n"+
    			"4;11;14;3;10;false\n";
    }
}
