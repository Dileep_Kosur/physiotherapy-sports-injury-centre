package DataAccess;

import Application.App;
import Entities.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class DataAccess {
    private List<Department> departmentList;
    private List<Physician> physicianList;
    private List<Patient> patientList;
    private List<Appointment> appointmentsList;
    private List<Appointment> cancelledAppointments;
    private List<Appointment> attendedAppointments;
    private List<Appointment> visitorAppointmentList;
    
    private static int physicianCounter=0;
    private static int patientCounter=0;
    private static int visitorCounter=0;
    private static int appointmentCounter=0;
    private static int treatmentCounter=0;
    private static int departmentCounter=0;
    
    public DataAccess() throws Exception {
        departmentList = new ArrayList<>();
        physicianList = new ArrayList<>();
        patientList = new ArrayList<>();
        appointmentsList = new ArrayList<>();
        cancelledAppointments = new ArrayList<>();
        attendedAppointments = new ArrayList<>();
        visitorAppointmentList=new ArrayList<>();
        loadDepartments();
        loadPhysicians();
        loadPatients();
        System.out.println();
        loadAppointments();
    }

    
    private void loadAppointments() {
    	String[] in = RawData.appointmentData().split("\n");
    	for(String line : in){
            if(line.length()<2) continue;
            String[] data = line.split(";");
            int treatmentId = Integer.parseInt(data[0]);
            int date = Integer.parseInt(data[1]);
            int hour= Integer.parseInt(data[2]);
            int physicianId= Integer.parseInt(data[3]);
            int patientId= Integer.parseInt(data[4]);
            boolean isVisitor=Boolean.parseBoolean(data[5]);
            appointmentsList.add(new Appointment(treatmentId, date, hour, physicianId, patientId, isVisitor,""));
        }
//    	System.out.print("test");
    }
    
    private void loadDepartments() {
        String[] in = RawData.departmentData().split("\n");
        for(String line : in){
            if(line.length()<2) continue;
            String[] data = line.split(";");
            String deptName = data[0];
            List<String> subDepts = new ArrayList<>(Arrays.asList(data).subList(1, data.length));
            departmentList.add(new Department(deptName,subDepts));
        }
    }
    private void loadPatients() {
        String[] in = RawData.patientData().split("\n");
        for(String line : in) {
            if (line.length() < 2) continue;
            String[] data = line.split(";");
//            int id = Integer.parseInt(data[0]);
            String name = data[0];
            String address = data[1];
            String telephone = data[2];
            Patient patient = new Patient(name,address,telephone);
            patientList.add(patient);
        }
    }

    private void loadPhysicians() throws Exception {
        String[] in = RawData.physicianData().split("\n");
        for(String line : in){
            if (line.length() < 2) continue;
            String[] data = line.split(";");
//            int id = Integer.parseInt(data[0]);
            String name = data[0];
            String address = data[1];
            String telephone = data[2];
            String hours = data[3];
            int[] areasOfExpertise = new int[data.length-4];
            for(int i=4;i< data.length;i++){
                if(isValidDepartment(data[i]))
                    areasOfExpertise[i-4] = getDepartmentByName(data[i]).getId();
            }
            physicianList.add(new Physician(
                    name,
                    address,
                    telephone,
                    areasOfExpertise,
                    hours
            ));
        }
    }
    private boolean isValidDepartment(String deptName) {
        return getDepartmentByName(deptName) != null;
    }
    public Department getDepartmentByName(String name){
        for(Department department : departmentList){
            if(name.equals(department.getName())){
                return department;
            }
        }
        return null;
    }
    
    public Physician getPhysicianByName(String name){
        for(Physician physician: physicianList){
            if(physician.getName().toLowerCase(Locale.ROOT).contains(name.toLowerCase(Locale.ROOT))) return physician;
        }
        return null;
    }

    public Physician getPhysicianById(int id){
        for(Physician physician: physicianList){
            if(physician.getId() == id) return physician;
        }
        return null;
    }
    public Patient getPatientById(int id){
        for(Patient patient : patientList){
            if(patient.getId() == id){
                return patient;
            }
        }
        return null;
    }
    
    public Patient getPatientByName(String name){
        for(Patient patient : patientList){
            if(patient.getName().toUpperCase().equals(name.toUpperCase())){
                return patient;
            }
        }
        return null;
    }
    
    public int registerPatient(String name, String address, String telephone) {
        Patient patient = new Patient(name, address, telephone);
        patientList.add(patient);
        return patient.getId();
    }

    public int registerPhysician(String name, String address, String telephone, int[] areaOfExpertiseIds, String hours) throws Exception {
        Physician physician = new Physician(name,address,telephone,areaOfExpertiseIds,hours);
        physicianList.add(physician);
        return physician.getId();
    }
    
    public List<Department> getDepartmentList() {
        return departmentList;
    }

    public List<Appointment> getAppointments() {
        return appointmentsList;
    }

    public List<Appointment> getCancelledAppointments(){
        return cancelledAppointments;
    }

    public void attendAppointment(Appointment appointment) {
        appointmentsList.remove(appointment);
        attendedAppointments.add(appointment);
    }

    public List<Appointment> getAttendedAppointments(){
        return attendedAppointments;
    }

    public List<Patient> getPatients(){
        return patientList;
    }
    public List<Physician> getPhysicians(){
        return physicianList;
    }
   

    public Department getDepartmentById(int id){
        for(Department department : departmentList){
            if(id == department.getId()){
                return department;
            }
        }
        return null;
    }

    public List<Physician> searchPhysiciansByDepartment(String departmentName){
        List<Physician> physicians = new ArrayList<>();
        int departmentId = getDepartmentByName(departmentName).getId();
        for(Physician physician: physicianList){
            if(physician.isExpertise(departmentId)){
                physicians.add(physician);
            }
        }
        return physicians;
    }
    
    public List<Appointment> fetchAppointmentsByPatientId(int id){
        List<Appointment> appointments = new ArrayList<>();
        for(Appointment appointment : appointmentsList){
            if(appointment.getPatientId() == id){
                appointments.add(appointment);
            }
        }
        return appointments;
    }


    public Appointment bookAppointment(int patientId,String name,
                                       int physicianId,
                                       int treatmentId,
                                       Integer date,
                                       Integer hour,
                                       boolean isVisitor) {
    	
        Appointment appointment = new Appointment(treatmentId,date,hour,physicianId,patientId,isVisitor,name);
        try {
            if (getPhysicianById(physicianId).blockSlot(date, hour)) {
            	
            		appointmentsList.add(appointment);
                    return appointment;
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public Treatment getTreatmentByID(int treatmentId) {
        for(Department department : departmentList){
            for(Treatment treatment : department.getTreatmentsAvailable()){
                if(treatment.getId() == treatmentId) return treatment;
            }
        }
        return null;
    }

    public void cancelAppointment(Appointment appointment) {
        int physicianId = appointment.getPhysicianId();
        int date = appointment.getDate();
        int hour = appointment.getHour();
        getPhysicianById(physicianId).unblockSlot(date,hour);
        appointmentsList.remove(appointment);
        cancelledAppointments.add(appointment);
    }


    public static int getAppointmentCounter() { return ++appointmentCounter;}

    public static int getPhysicianCounter() {
        return ++physicianCounter;
    }

    public static int getPatientCounter() {
        return ++patientCounter;
    }

    public static int getVisitorCounter() {
        return ++visitorCounter;
    }
    
    public static int getTreatmentCounter() {
        return ++treatmentCounter;
    }
    
    public static int getDepartmentCounter() {
        return ++departmentCounter;
    }
}
