package Utils;

public class Parser {

    private static int hoursToInt(String hour) throws Exception {
        int num = Integer.parseInt(hour.substring(0,hour.length()-2));
        String ampm = hour.substring(hour.length()-2);

        if(
                ((!ampm.equals("am")) && (!ampm.equals("pm")))
                ||
                num <= 0 || num > 12
        ){
            throw new Exception("Invalid consultation hours format!");
        }

        if(ampm.equals("pm") && num!=12){
            num += 12;
        }

        return num;

    }

    public static boolean[][] parseConsultationHours(String weeklyConsultationHoursString, int numDays) throws Exception {

        weeklyConsultationHoursString = weeklyConsultationHoursString.replaceAll(" ","");

        String[] times = weeklyConsultationHoursString.split(",");

        if(times.length!=1 && times.length!=7){
            throw new Exception("Invalid consultation hours format!");
        }
        int indexManager = Integer.MAX_VALUE;
        if(times.length==1){
            indexManager = 0;
        }

        boolean[][] consultationHours = new boolean[numDays][24];

        for(int day=0;day<7;day++){
            String[] hours = times[Integer.min(day,indexManager)].split("-");
            int hour1 = hoursToInt(hours[0]);
            int hour2 = hoursToInt(hours[1]);
            if(hour2<=hour1){
                throw new Exception("Invalid consultation hours!");
            }
            for(int hour=hour1;hour<=hour2;hour++){
                for(int weekday = day; weekday < consultationHours.length; weekday++)
                    consultationHours[weekday][hour] = true;
            }
        }

        return consultationHours;

    }

    public static String parseIndexToHour(Integer hr) {
        if(hr==0) return "12 AM";
        if(hr<12) return hr + " AM";
        if(hr==12) return "12 PM";
        return (hr-12) + " PM";
    }
}
