package Application;

import DataAccess.DataAccess;
import Entities.*;
import Utils.Parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class App{

    private Personnel activeUser = null;
    private Physician selectedPhysician = null;
    private Department selectedDepartment = null;
    private Integer selectedDate = null;
    private Integer selectedSlot = null;
    private Treatment selectedTreatment = null;

    private final DataAccess dataAccess;
    private final Scanner scanner;

    public App() throws Exception {
        dataAccess = new DataAccess();
        scanner = new Scanner(System.in);        
        mainLoginScreen();
    }
    private void mainLoginScreen() {
        int input=-1;
        do{        	
        	System.out.println("Welcome to PSIC Booking System!");
        	System.out.println();
            System.out.println("Press 1 to continue as a Patient");
            System.out.println("Press 2 to continue as a Visitor");
            System.out.println("Press 3 to generate report by Appointments and Treatments");
            System.out.println("Press 4 to generate report by each Patient");
            System.out.println("Press -1 to quit");
	        try{
	        	input = scanner.nextInt();
	        }catch (Exception e) {
	        	System.out.println("Error in reading input\n");
	        	scanner.next();
			}
	        
	        switch(input) {
	        	case 1:
	        		patientScreen();
	        		break;
	        	case 2:
	        		visitorLoginScreen();
	        		break;
	        	case 3:
	        		reportByTreatments();
	        		break;
	        	case 4:
	        		reportByPatient();
	        		break;
	        	case -1:
	        		System.exit(0);
	        		break;
	        	default:
	        		System.out.println("Kindly enter valid values from 1 to 3 or -1 to exit");
	                break;
	        }
        }while(input != -1);
    }
    
    private void patientScreen() {
    	int input = -1;
        do {
        	System.out.println();
        	System.out.println("Welcome to Patient Section");
        	System.out.println();
            System.out.println("Press 1 to login as a Patient");
            System.out.println("Press 2 if you don't have an account");
            System.out.println("Press -1 to go back");           
	        try{
	        	input = scanner.nextInt();
	        }catch (Exception e) {
	        	System.out.println("Error in reading input\n");
	        	scanner.next();
			}
	        switch (input) {
	            case 1:
	            	patientLoginScreen();
	            	break;
	            case 2:
	            	patientRegisterScreen();
	            	break;
	            case -1:
	            	break;
	            default: {
	                errorMessage();
	                patientScreen();
	            }
	        }
        }while(input != -1);
    }
    
    private void patientLoginScreen() {
        int x = -1;
        do {
        	System.out.println();
        	System.out.println("Patient Login Section...");
        	System.out.println();
            System.out.println("Press 1 to login with your id");
            System.out.println("Press 2 to login with your name");
            System.out.println("Press -1 to go back");
        	try{
            	x = scanner.nextInt();
            }catch (Exception e) {
            	System.out.println("Error in reading input\n");
            	scanner.next();
    		}
            
            switch (x) {
                case 1: {
                    System.out.println("Please enter your id: ");
                    String input = scanner.next();
                    try {
                        int id = Integer.parseInt(input);
                        activeUser = dataAccess.getPatientById(id);
                        if (activeUser == null) {
                        	System.out.println("Sorry ... Invalid Id");
//                            patientLoginScreen();
                        } else {
                            patientResultScreen();
                        }
                    } catch (Exception e) {
                        errorMessage();
                        patientLoginScreen();
                    }
                }
                break;
                case 2: {
                    System.out.println("Please enter your name: ");
                    String name = "";
                    while (name.length() == 0) name = scanner.nextLine();
                    activeUser = dataAccess.getPatientByName(name);
                    if (activeUser == null) {
                    	System.out.println("Sorry ..Invalid Name");
//                        patientLoginScreen();
                    } else {
                        patientResultScreen();
                    }
                }
                break;
                case -1:
                	break;
                default : {
                    errorMessage();
                    patientLoginScreen();
                }
            }
        }while(x!=-1);
        
    }
    
    private void patientResultScreen() {
        
        int x = -1;
        do {
        	System.out.println();
            System.out.println("Appointment Section : ");
            System.out.println();
            System.out.println("Press 1 to book an appointment");
            System.out.println("Press 2 to update a booked appointment");
            System.out.println("Press 3 to attend a booked appointment");
            System.out.println("Press 4 to logout and go to main page");
            System.out.println("Press -1 to go back");
        	try{
            	x = scanner.nextInt();
            }catch (Exception e) {
            	System.out.println("Error in reading input\n");
            	scanner.next();
    		}
            switch (x) {
                case 1:
                	bookPatientAppointmentScreen();
                	break;
                case 2:
                	updatePatientAppointmentScreen();
                	break;
                case 3:
                	attendAppointmentScreen();
                	break;
                case 4:
	                {
	                    activeUser = null;
	                    mainLoginScreen();
	                }
                	break;
                case -1: 
                	break;
                default :
                {
                    errorMessage();
                    patientResultScreen();
                }
            }
        }while(x!=-1);
    }
    private void attendAppointmentScreen() {
        System.out.println();        
        List<Appointment> appointments = dataAccess.fetchAppointmentsByPatientId(activeUser.getId());
        if(appointments.size() >0) {
        	System.out.println("Fetching your appointments...");
            System.out.println("Select the appointment to Attend...");
            for(int i=0;i< appointments.size();i++){
                Appointment app = appointments.get(i);
                System.out.println((i+1)+
                		". Date: "+(app.getDate()+1)+
                		", Time: "+Parser.parseIndexToHour(app.getHour())+
                		", with "+dataAccess.getPhysicianById(app.getPhysicianId()).getName()+
                		", Treatment: "+dataAccess.getTreatmentByID(app.getTreatmentId()).getName()+ 
                		", in : "+dataAccess.getTreatmentByID(app.getTreatmentId()).getRoomName());
            }
            int indexSelected = scanner.nextInt()-1;
            Appointment appointment = appointments.get(indexSelected);

            System.out.println("Are you sure you want to confirm that you have attended the appointment?");
            System.out.println("Press 1 to confirm");
            System.out.println("Press any other key to return back");
            try{
            	String x= scanner.next();
            	 if(x.equals("1")){
                     dataAccess.attendAppointment(appointment);
                     System.out.println("Attended the appointment successfully! Taking you to the main page!");
                 }
            }catch (Exception e) {
            	System.out.println("Error in reading input\n");
            	scanner.next();
    		}
              
        }
        else {
        	System.out.println("Sorry ...No appointments found for you");
        	System.out.println();
        }
    }

    private void updatePatientAppointmentScreen() {
        System.out.println();
        List<Appointment> appointments = dataAccess.fetchAppointmentsByPatientId(activeUser.getId());
        if(appointments.size()>0) {
        	System.out.println("Fetching your appointments...");
            System.out.println("Select the appointment to cancel...");
        	for(int i=0;i< appointments.size();i++){
                Appointment app = appointments.get(i);
                System.out.println((i+1)+". Date: "+(app.getDate()+1)+
                		", Time: "+Parser.parseIndexToHour(app.getHour())+
                		", with "+dataAccess.getPhysicianById(app.getPhysicianId()).getName()+
                		", Treatment:"+dataAccess.getTreatmentByID(app.getTreatmentId()).getName()+ 
                		", in :"+dataAccess.getTreatmentByID(app.getTreatmentId()).getRoomName());
            }
            int indexSelected = scanner.nextInt()-1;
            Appointment appointment = appointments.get(indexSelected);
            
            System.out.println("Are you sure you want to cancel the appointment?");
            System.out.println("Press 1 to confirm");
            System.out.println("Press any other key to return back");
            
            try{
            	String x= scanner.next();
            	 if(x.equals("1")){
            		 dataAccess.cancelAppointment(appointment);
                     System.out.println("Cancelled the appointment successfully! Taking you to the main page!");
                 }
            }catch (Exception e) {
            	System.out.println("Error in reading input\n");
            	scanner.next();
    		}
        }
        else
        {
        	System.out.println("Sorry ...No appointments found for you");
        	System.out.println();
        }
    }

    private void bookPatientAppointmentScreen() {

        selectedDate = null;
        selectedDepartment = null;
        selectedPhysician = null;
        selectedSlot = null;
        selectedTreatment = null;
        int x = -1;
        do {
        	System.out.println();
            System.out.println("Press 1 to search physicians by area of expertise");
            System.out.println("Press 2 to search physician by name");
            System.out.println("Press 3 to logout");
            System.out.println("Press -1 to go back");
            try{
            	x = scanner.nextInt();
            }catch (Exception e) {
            	System.out.println("Error in reading input");
            	scanner.next();
    		}
            
            switch (x) {
                case 1: 
                	searchPhysicianByAreaOfExpertiseScreen();
                	break;
                case 2: 
                	searchPhysicianByNameScreen();
                	break;
                case 3:
                	{
    	                activeUser = null;
    	                mainLoginScreen();
                	}
                	break;
                case -1:
                	break;
                default :{
                    errorMessage();
                    patientResultScreen();
                }
            }
        }while(x!=-1);
    }
    
    private void searchPhysicianByNameScreen() {
        System.out.println();
        System.out.println("Enter the name of the physician: ");
        String name = "";
        while(name.length()==0) name = scanner.nextLine();
        selectedPhysician = dataAccess.getPhysicianByName(name);
        if(selectedPhysician == null){
            System.out.println("Sorry no Physician found");
            bookPatientAppointmentScreen();
        }else{
            selectDepartmentScreen();
        }
    }

    private void searchPhysicianByAreaOfExpertiseScreen() {
        System.out.println();
        System.out.println("Select an area of expertise (enter the index)");
        List<Department> departments = dataAccess.getDepartmentList();
        for(int i=0;i<departments.size();i++){
            System.out.println((i+1)+". "+departments.get(i).getName());
        }
        try {
        	int indexSelected = scanner.nextInt() - 1;
            selectedDepartment = departments.get(indexSelected);
            selectPhysicianScreen(dataAccess.searchPhysiciansByDepartment(selectedDepartment.getName()));
        } catch (Exception e) {
            errorMessage();
            scanner.next();
            bookPatientAppointmentScreen();
        }
    }
    
    private void selectPhysicianScreen(List<Physician> physicians) {
        System.out.println();
        System.out.println("Select a physician (enter the index)");
        for(int i=0;i< physicians.size();i++){
            System.out.println((i+1)+".\n");
            printPhysicianData(physicians.get(i));
            System.out.println();
        }
        try {
        	int indexSelected = scanner.nextInt() - 1;
            selectedPhysician = physicians.get(indexSelected);
            showPhysicianOptions();
        } catch (Exception e) {
            errorMessage();
            scanner.next();
            bookPatientAppointmentScreen();
        }
    }

    private void printPhysicianData(Physician physician) {
        System.out.println("Name: "+physician.getName());

        System.out.println("Available time:");
        String[] consHours = physician.getConsultationHoursString().split(",");
        String[] weekDays = new String[]{"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
        if(consHours.length==1){
            String[] hours = new String[7];
            for(int i=0;i<7;i++) hours[i] = consHours[0];
            consHours = hours;
        }
        for(int i=0;i<7;i++){
            System.out.println(weekDays[i]+": "+consHours[i]);
        }
        System.out.println();
        System.out.println("Treatments available: ");
        int[] departmentId = physician.getAreaOfExpertise();
        for(int id: departmentId){
            Department department = dataAccess.getDepartmentById(id);
            for(Treatment treatment : department.getTreatmentsAvailable()){
                System.out.println(treatment.getName()+", Room: "+treatment.getRoomName());
            }
        }
    }

    private void selectDepartmentScreen() {
        System.out.println();
        System.out.println("Select an area of expertise (enter the index)");
        int[] departments = selectedPhysician.getAreaOfExpertise();
        for(int i=0;i<departments.length;i++){
            System.out.println((i+1)+". "+dataAccess.getDepartmentById(departments[i]).getName());
        }
        try {
        	int indexSelected = scanner.nextInt() - 1;
            selectedDepartment = dataAccess.getDepartmentById(departments[indexSelected]);
            System.out.println(selectedDepartment.getName());
            showPhysicianOptions();
        } catch (Exception e) {
            errorMessage();
            scanner.next();
            bookPatientAppointmentScreen();
        }
    }

    private void showPhysicianOptions(){
        System.out.println();
        System.out.println("Enter a date (1-31) to book an appointment");
        selectedDate = scanner.nextInt() - 1;
        List<Integer> slotsAvailable = selectedPhysician.getSlotsAvailable(selectedDate);
        System.out.println("Select a slot. Slots available on date "+(selectedDate+1)+" are: ");
        for(int i=0;i< slotsAvailable.size();i++){
            System.out.println((i+1)+". "+ Parser.parseIndexToHour(slotsAvailable.get(i)));
        }
        try{
        	int indexSelected = scanner.nextInt() - 1;
            selectedSlot = slotsAvailable.get(indexSelected);
        } catch (Exception e) {
            errorMessage();
            scanner.next();
            bookPatientAppointmentScreen();
            return;
        }
        showTreatmentOptions();
    }

    private void showTreatmentOptions() {
        Treatment[] treatments = selectedDepartment.getTreatmentsAvailable();
        System.out.println();
        System.out.println("Select a treatment from below (select the index)");
        for(int i=0;i< treatments.length;i++){
            System.out.println((i+1)+". "+treatments[i].getName());
        }
        
        try{
        	int index = scanner.nextInt() - 1;
            selectedTreatment = treatments[index];
            bookAppointmentScreen();
        } catch (Exception e) {
            errorMessage();
            scanner.next();
            bookPatientAppointmentScreen();
        }
    }

    private void bookAppointmentScreen() {
        Appointment appointment;
        if((appointment = dataAccess.bookAppointment(activeUser.getId(),activeUser.getName(),
                                    selectedPhysician.getId(),
                                    selectedTreatment.getId(),
                                    selectedDate,
                                    selectedSlot,
                                    activeUser.getClass() == Visitor.class))!=null){
            System.out.println("Successfully booked!");
            System.out.println("Your appointment details are: ");
            System.out.println("Appointment Number: "+appointment.getId());
            if(activeUser.getClass() == Patient.class) {
                System.out.println("Patient Name: " + activeUser.getName());
                System.out.println("Patient ID: " + activeUser.getId());
            }
            else{
                System.out.println("Visitor Name: "+activeUser.getName());
            }
            System.out.println("Physician Name: "+dataAccess.getPhysicianById(appointment.getPhysicianId()).getName());
            System.out.println("Physician ID: "+dataAccess.getPhysicianById(appointment.getPhysicianId()).getId());
            System.out.println("Date: "+(appointment.getDate()+1));
            System.out.println("Time: "+Parser.parseIndexToHour(appointment.getHour()));
            System.out.println("Treatment name: "+dataAccess.getTreatmentByID(appointment.getTreatmentId()).getName());
            System.out.println("Place: "+dataAccess.getTreatmentByID(appointment.getTreatmentId()).getRoomName());

            System.out.println();
            System.out.println("Logging you out...");
            activeUser = null;
            selectedPhysician = null;
            selectedDepartment = null;
            selectedTreatment = null;
            selectedDate = null;
            selectedSlot = null;

            mainLoginScreen();
        }
        else{
            errorMessage();
            bookPatientAppointmentScreen();
        }
    }

    private void visitorLoginScreen() {
    	System.out.println();
        System.out.println("WARNING: A visitor's details won't be saved in our database. You can't update the appointment later!");
        System.out.println("Press 1 to continue as a visitor");
        System.out.println("Press 2 to go to the main page");
        int x = -1;
        try{
        	x = scanner.nextInt();
        }catch (Exception e) {
        	System.out.println("Error in reading input");
        	scanner.next();
		}
        
        switch (x) {
            case 1: {
                System.out.println();
                System.out.println("Please enter your name, it won't be saved in our database: ");
                try {
                	String name = scanner.next();
                    activeUser = new Visitor(name);
                    bookPatientAppointmentScreen();
                }catch(Exception e) {
                	System.out.println("Error in reading input");
                	scanner.next();
                }
            }
            case 2: mainLoginScreen();
            default: {
                errorMessage();
                visitorLoginScreen();
            }
        }
    }
    
    private void patientRegisterScreen() {
        System.out.println("Patient Registeration Section !!!");
        System.out.println("Enter your name: ");
        String name="";
        while(name.length()==0) name = scanner.nextLine();
        System.out.println("Enter your address: ");
        String address = "";
        while(address.length()==0) address = scanner.nextLine();
        System.out.println("Enter your telephone: ");
        String telephone = "";
        while(telephone.length()==0) telephone = scanner.nextLine();

        int id = dataAccess.registerPatient(name,address,telephone);
        System.out.println("Successfully registered you as a patient, your id is: "+id);
        System.out.println();
        System.out.println("Redirecting you .....");
//        patientLoginScreen();
    }
    private void errorMessage(){
        System.out.println();
        System.out.println("Sorry, that didn't work, please try again...");
    }

    
    
    private void reportByTreatments(){
        System.out.println();
        System.out.println("Generating appointment lists: ");

        List<Appointment> appointments = dataAccess.getAppointments();
        List<Appointment> cancelledAppointments = dataAccess.getCancelledAppointments();
        List<Appointment> attendedAppointments = dataAccess.getAttendedAppointments();
        System.out.println();
        System.out.println("Report 1: ALL APPOINTMENTS");
        System.out.println("********************************************************************************");
        System.out.println("UNATTENDED APPOINTMENTS: ");
        boolean temp = false;
        for(int i=0;i<appointments.size();i++){
            temp = true;
            System.out.println("***************************************");
            Appointment app = appointments.get(i);
            if(app.isVisitor()==false) {
            	System.out.println((i+1)+
                        ". Appointment ID:"+app.getId()+", \n"+
                        "Patient Name: "+ dataAccess.getPatientById(app.getPatientId()).getName()+", \n"+
                        "Date: "+(app.getDate()+1)+", \n"+
                        "Time: "+Parser.parseIndexToHour(app.getHour())+", \n"+
                        "Physician: "+dataAccess.getPhysicianById(app.getPhysicianId()).getName()+", \n"+
                        "Treatment: "+dataAccess.getTreatmentByID(app.getTreatmentId()).getName()+", \n"+
                        "Room: "+dataAccess.getTreatmentByID(app.getTreatmentId()).getRoomName());
            }
            else {
            }
        }
        if(!temp) System.out.println("No appointments found!\n");
        
        
        System.out.println();
        System.out.println();
        temp = false;
        System.out.println("ATTENDED APPOINTMENTS:");
        for(int i=0;i<attendedAppointments.size();i++){
            temp = true;
            System.out.println("***************************************");
            Appointment app = attendedAppointments.get(i);
            System.out.println((i+1)+
                    ". Appointment ID:"+app.getId()+", \n"+
                    "Patient Name: "+ dataAccess.getPatientById(app.getPatientId()).getName()+", \n"+
                    "Date: "+(app.getDate()+1)+", \n"+
                    "Time: "+Parser.parseIndexToHour(app.getHour())+", \n"+
                    "Physician: "+dataAccess.getPhysicianById(app.getPhysicianId()).getName()+", \n"+
                    "Treatment: "+dataAccess.getTreatmentByID(app.getTreatmentId()).getName()+", \n"+
                    "Room: "+dataAccess.getTreatmentByID(app.getTreatmentId()).getRoomName()+"\n");
        }
        if(!temp) System.out.println("No appointments found!\n");
        
        System.out.println();
        System.out.println();
        System.out.println("CANCELLED APPOINTMENTS:");
        System.out.println();
        for(int i=0;i<cancelledAppointments.size();i++){
            temp = true;
            System.out.println("***************************************");
            Appointment app = cancelledAppointments.get(i);
            System.out.println((i+1)+
                    ". Appointment ID:"+app.getId()+", \n"+
                    "Patient Name: "+ dataAccess.getPatientById(app.getPatientId()).getName()+", \n"+
                    "Date: "+(app.getDate()+1)+", \n"+
                    "Time: "+Parser.parseIndexToHour(app.getHour())+", \n"+
                    "Physician: "+dataAccess.getPhysicianById(app.getPhysicianId()).getName()+", \n"+
                    "Treatment: "+dataAccess.getTreatmentByID(app.getTreatmentId()).getName()+", \n"+
                    "Room: "+dataAccess.getTreatmentByID(app.getTreatmentId()).getRoomName()+"\n");
        }
        if(!temp) System.out.println("No appointments found!\n");
        
        System.out.println("VISITOR CONSULTATIONS");
        System.out.println("***************************************");
        for(int i=0;i<appointments.size();i++){
            temp = true;
            
            Appointment app = appointments.get(i);
            if(app.isVisitor()==false) {
//            	System.out.println((i+1)+
//                        ". Appointment ID:"+app.getId()+", \n"+
//                        "Patient Name: "+ dataAccess.getPatientById(app.getPatientId()).getName()+", \n"+
//                        "Date: "+(app.getDate()+1)+", \n"+
//                        "Time: "+Parser.parseIndexToHour(app.getHour())+", \n"+
//                        "Physician: "+dataAccess.getPhysicianById(app.getPhysicianId()).getName()+", \n"+
//                        "Treatment: "+dataAccess.getTreatmentByID(app.getTreatmentId()).getName()+", \n"+
//                        "Room: "+dataAccess.getTreatmentByID(app.getTreatmentId()).getRoomName());
            }
            else {
            	System.out.println((i+1)+
                        ". Appointment ID:"+app.getId()+", \n"+
                        "Visitor Name: "+ app.getNotes()+", \n"+
                        "Date: "+(app.getDate()+1)+", \n"+
                        "Time: "+Parser.parseIndexToHour(app.getHour())+", \n"+
                        "Physician: "+dataAccess.getPhysicianById(app.getPhysicianId()).getName()+", \n"+
                        "Treatment: "+dataAccess.getTreatmentByID(app.getTreatmentId()).getName()+", \n"+
                        "Room: "+dataAccess.getTreatmentByID(app.getTreatmentId()).getRoomName()+"\n");
            }
        }
    }
    
    private void reportByPatient() {
    	System.out.println();
        System.out.println("Generating appointment lists: ");

        List<Appointment> appointments = dataAccess.getAppointments();
        List<Appointment> cancelledAppointments = dataAccess.getCancelledAppointments();
        List<Appointment> attendedAppointments = dataAccess.getAttendedAppointments();
        System.out.println();
        boolean temp = false;
        
        System.out.println("Report : APPOINTMENTS BY EACH PATIENTS");
        for(Patient patient : dataAccess.getPatients()){
            System.out.println("*************************************************************");
            System.out.println("Patient ID: "+patient.getId());
            System.out.println("Patient Name: "+patient.getName());
            System.out.println();
            int i=1;
            System.out.println("UNATTENDED APPOINTMENTS PRESENT: ");
            temp = false;
            System.out.println("***************************************");
            for(Appointment app : appointments){                
                if(app.getPatientId()==patient.getId()) {
                    System.out.println((i++) +
                            ". Appointment ID:" + app.getId() + ", \n" +
                            "Date: " + (app.getDate() + 1) + ", \n" +
                            "Time: " + Parser.parseIndexToHour(app.getHour()) + ", \n" +
                            "Physician: " + dataAccess.getPhysicianById(app.getPhysicianId()).getName() + ", \n" +
                            "Treatment: " + dataAccess.getTreatmentByID(app.getTreatmentId()).getName() + ", \n" +
                            "Room: " + dataAccess.getTreatmentByID(app.getTreatmentId()).getRoomName()+"\n");
                    temp = true;
                }
            }
            if(!temp) System.out.println("No pending appointments found for this patient!\n");

            temp = false;
            System.out.println("ATTENDED APPOINTMENTS: ");
            for(Appointment app: attendedAppointments){
                System.out.println("***************************************");
                if(app.getPatientId()==patient.getId()) {
                    System.out.println((i++) +
                            ". Appointment ID:" + app.getId() + ", \n" +
                            "Date: " + (app.getDate() + 1) + ", \n" +
                            "Time: " + Parser.parseIndexToHour(app.getHour()) + ", \n" +
                            "Physician: " + dataAccess.getPhysicianById(app.getPhysicianId()).getName() + ", \n" +
                            "Treatment: " + dataAccess.getTreatmentByID(app.getTreatmentId()).getName() + ", \n" +
                            "Room: " + dataAccess.getTreatmentByID(app.getTreatmentId()).getRoomName()+"\n");
                    temp = true;
                }
            }
            if(!temp) System.out.println("No attended appointments found for this patient!\n");

            temp = false;
            System.out.println("CANCELLED APPOINTMENTS: ");
            for(Appointment app: cancelledAppointments){
                System.out.println("***************************************");
                if(app.getPatientId()==patient.getId()) {
                    System.out.println((i++) +
                            ". Appointment ID:" + app.getId() + ", \n" +
                            "Date: " + (app.getDate() + 1) + ", \n" +
                            "Time: " + Parser.parseIndexToHour(app.getHour()) + "\n" +
                            "Physician: " + dataAccess.getPhysicianById(app.getPhysicianId()).getName() + ", \n" +
                            "Treatment: " + dataAccess.getTreatmentByID(app.getTreatmentId()).getName() + ", \n" +
                            "Room: " + dataAccess.getTreatmentByID(app.getTreatmentId()).getRoomName()+"\n");
                    temp = true;
                }
            }
            if(!temp) System.out.println("No cancelled appointments found for this patient!\n");
        }
    }
    public static void main(String[] args) throws Exception {
        new App();
    }
}
